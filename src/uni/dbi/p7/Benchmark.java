package uni.dbi.p7;

import uni.dbi.p7.benchmarks.*;
import uni.dbi.p7.data.BenchmarkData;
import uni.dbi.p7.data.DataProvider;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Benchmark {

    /**
     * Aktive Benchmark-Klassen, die verwendet werden können
     */
    private static final Class<?>[] benchmarkClasses = new Class<?>[]{
            PreparedStatementAutoCommitBenchmark.class,
            PreparedStatementAutoCommitBatchBenchmark.class,
            CSVBenchmark.class,
            SingleStatementBenchmark.class,
            PreparedStatementBenchmark.class
    };

    /**
     * Führt einen neuen Benchmark mit den übergebenen Parametern durch
     *
     * @param benchmark Implementierung für den Benchmark
     * @param connStr   Connection-String (JDBC-Url)
     * @param data      Daten für den Benchmark
     * @return Die Zeit in Nanosekunden, die der Benchmark gebraucht hat
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public long benchmark(AbstractBenchmark benchmark, String connStr, BenchmarkData data) throws ClassNotFoundException, SQLException {
        // Die Klasse für den h2 Treiber laden
        Class.forName("org.h2.Driver");
        // Verbindung zur Datenbank aufbauen (als Paramater wird der Connection-String, Benutzername und Passwort übertragen)
        Connection connection = DriverManager.getConnection(connStr, "sa", null);
        // Das Connection Objekt an die Benchmark Instanz übergeben
        benchmark.setConnection(connection);

        // Datenbank für den Test vorbereiten
        benchmark.initDB();

        // Benchmark durchführen
        return benchmark.run(data);
    }

    public static void main(String[] args) {
        // Hilfe ausgeben, wenn Übergabegabeparameter fehlen
        if (args.length == 0) {
            System.out.println("Parameter 1: Connection String");
            System.out.println("Parameter 2: n");
            System.out.println("Parameter 3: Wiederholungen");
            System.out.println("Parameter 4: Benchmark Klasse");

            for (Class<?> c : benchmarkClasses) {
                System.out.println("  - " + c.getSimpleName());
            }
            return;
        }

        // Benchmark instanziieren
        Benchmark benchmark = new Benchmark();

        String connectionString = args[0]; // JDBC-Url
        int n = Integer.parseInt(args[1]); // n-Tupel
        int count = Integer.parseInt(args[2]); // Anzahl der Durchläufe
        Class<?> benchmarkClass = benchmarkClasses[Integer.parseInt(args[3])];

        // Gewählte Einstellungen für den Benchmark ausgeben
        System.out.println("Connection: " + connectionString);
        System.out.println("n: " + n);
        System.out.println("Repeats: " + count);
        System.out.println("Benchmark Class: " + benchmarkClass.getSimpleName());

        // Datensätze für Benchmark besorgen
        DataProvider dataProvider = new DataProvider();
        BenchmarkData data = dataProvider.getBenchmarkData(n);

        try {
            long sum = 0; // Gesamtzeit (zur späteren Berechnung des Durchschnitts)

            for (int i = 0; i < count; i++) {
                // Gewählte Benchmark-Klasse instanzieren
                Constructor<?> constructor = benchmarkClass.getConstructor(Connection.class);
                AbstractBenchmark benchmarkImplementation = (AbstractBenchmark) constructor.newInstance((Connection) null);

                // Benchmark durchführen
                long duration = benchmark.benchmark(benchmarkImplementation, connectionString, data);
                sum += duration;

                System.out.println();
                System.out.print("Time: " + (duration / 1000000) + "ms");
            }

            System.out.println();

            if (count > 1) {
                // Durchschnittszeit ausgeben
                System.out.println("Avg: " + ((sum / count) / 1000000) + "ms");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
