package uni.dbi.p7.benchmarks;

import uni.dbi.p7.SQLStatements;
import uni.dbi.p7.data.BenchmarkData;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Superklasse für verschiedene Benchmark-Implementierungen.
 * Kümmert sich zusätzlich um allgemeine Dinge wie die Vorbereitung der Datenbank.
 */
public abstract class AbstractBenchmark {

    protected Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    protected AbstractBenchmark(Connection connection) {
        setConnection(connection);
    }

    /**
     * Bereitet die Datenbank vor, indem sie zunächst geleert wird
     * und anschließend die notwendigen Tabellen erzeugt werden.
     */
    public void initDB() throws SQLException {
        clearDatabase();
        createSchema();
    }

    /**
     * Setzt die Datenbank bzw. dessen Inhalte zurück.
     */
    private void clearDatabase() throws SQLException {
        String[] tables = new String[]{"Branches", "Tellers", "Accounts", "History"};  // Tabellen die geleert werden sollen
        for (String table : tables) {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("DROP TABLE IF EXISTS " + table);
        }
    }

    /**
     * Datenbankschema anlegen
     */
    protected void createSchema() throws SQLException {
        Statement stmt = connection.createStatement();
        stmt.executeUpdate(SQLStatements.CREATE_TABLES);
    }

    /**
     * Übergebene Daten einfügen
     * @param data Testdaten
     */
    protected abstract void insertData(BenchmarkData data) throws SQLException;

    /**
     * Führt den Benchmark durch.
     * @param data Daten für den Benchmark
     * @return Benötigte Zeit in Nanosekunden
     */
    public long run(BenchmarkData data) throws SQLException {
        // Beziehungen sollen für das Einfügen der Daten nicht geprüft werden
        getStatement().executeUpdate("SET REFERENTIAL_INTEGRITY FALSE");

        // Zeitmessung
        long start = System.nanoTime();

        // Aufruf der konkreten Implementierung zum Einfügen der Daten
        insertData(data);

        // Zeitmessung
        long result = System.nanoTime() - start;

        // Später sollen Beziehungen geprüft werden
        getStatement().executeUpdate("SET REFERENTIAL_INTEGRITY TRUE");

        return result;
    }

    /**
     * Erzeugt ein neues Statement zurück.
     */
    protected Statement getStatement() throws SQLException {
        return connection.createStatement();
    }
}
