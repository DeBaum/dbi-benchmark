package uni.dbi.p7.benchmarks;

import uni.dbi.p7.data.BenchmarkData;
import uni.dbi.p7.data.entities.Account;
import uni.dbi.p7.data.entities.Branch;
import uni.dbi.p7.data.entities.Teller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Befüllt die Datenbank mithilfe eines (lokalen) Imports aus einer CSV-Datei.
 * <p/>
 * Dabei wird zunächst die CSV-Datei in einem temporären Ordner erstellt und
 * anschließend an die Datenbank übergeben, welche die Werte aus der Datei
 * ausliest und in die entsprechenden Tabellen einfügt.
 * <p/>
 * Wichtig: Derzeit werden nur die Accounts per CSV importiert, während
 * die übrigen Tabellen wie beim PreparedStatementAutoCommitBenchmark befüllt
 * werden.
 */
public class CSVBenchmark extends AbstractBenchmark {

    // Trennzeichen für die CSV-Datei
    private static final String CSV_DELIMTER = ",";

    public CSVBenchmark(Connection connection) {
        super(connection);
    }

    @Override
    protected void createSchema() throws SQLException {
        /*
        Die createSchema()-Methode wird als Workaround überschrieben,
        um die Tabelle "Accounts" nachträglich zu löschen, da der Import
        per CSV die Tabelle direkt miterzeugt.
        */
        super.createSchema();
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DROP TABLE IF EXISTS Accounts");
    }

    @Override
    protected void insertData(BenchmarkData data) throws SQLException {
        PreparedStatement stmt;

        // auto-commit deaktivieren
        boolean autoCommit = connection.getAutoCommit();
        connection.setAutoCommit(false);

        stmt = connection.prepareStatement(
                "INSERT INTO Branches (branchId, balance, branchName, address) " +
                        "VALUES(?, ?, ?, ?)"
        );

        for (int i = 0, max = data.Branches.length; i < max; i++) {
            Branch branch = data.Branches[i];
            stmt.clearParameters();
            stmt.setInt(1, branch.BranchId);
            stmt.setInt(2, branch.Balance);
            stmt.setString(3, branch.Branchname);
            stmt.setString(4, branch.Address);

            stmt.executeUpdate();
        }

        try {
            // CSV Datei für die Accounts erstellen
            File file = File.createTempFile("accounts-", "-csv");
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0, max = data.Accounts.length; i < max; i++) {
                Account account = data.Accounts[i];
                writer.write(String.valueOf(account.AccId) + CSV_DELIMTER
                        + account.Name + CSV_DELIMTER
                        + account.Balance + CSV_DELIMTER
                        + account.BranchId + CSV_DELIMTER
                        + account.Address
                        + "\n");
            }

            Statement updateStmt = connection.createStatement();
            updateStmt.executeUpdate("create table accounts " +
                    "(  accid int not null, " +
                    "name char(20) not null,   " +
                    "balance int not null,  " +
                    "branchid int not null, " +
                    "address char(68) not null, " +
                    "primary key (accid), " +
                    "foreign key (branchid) references branches ) " +
                    "    AS SELECT * FROM CSVREAD('" + file.getAbsolutePath() + "');");
        } catch (IOException e) {
            e.printStackTrace();
        }

        stmt = connection.prepareStatement(
                "INSERT INTO Tellers (tellerId, balance, branchId, tellerName, address)" +
                        "VALUES (?, ?, ?, ?, ?) ");

        for (int i = 0, max = data.Tellers.length; i < max; i++) {
            Teller teller = data.Tellers[i];
            stmt.clearParameters();
            stmt.setInt(1, teller.TellerId);
            stmt.setInt(2, teller.Balance);
            stmt.setInt(3, teller.BranchId);
            stmt.setString(4, teller.Name);
            stmt.setString(5, teller.Address);

            stmt.executeUpdate();
        }

        connection.commit(); // Änderungen speichern
        connection.setAutoCommit(autoCommit);
    }
}
