package uni.dbi.p7.benchmarks;

import uni.dbi.p7.data.BenchmarkData;
import uni.dbi.p7.data.entities.Account;
import uni.dbi.p7.data.entities.Branch;
import uni.dbi.p7.data.entities.Teller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Befüllt die Datenbank mithilfe von PreparedStatements.
 * <p/>
 * Zusätzlich wird der "auto-commit"-Modus der Verbindung deaktiviert und die Änderungen
 * werden erst am Ende zum Server geschickt.
 */
public class PreparedStatementAutoCommitBenchmark extends AbstractBenchmark {

    public PreparedStatementAutoCommitBenchmark(Connection connection) {
        super(connection);
    }

    @Override
    protected void insertData(BenchmarkData data) throws SQLException {
        PreparedStatement stmt;

        // auto-commit deaktivieren
        boolean autoCommit = connection.getAutoCommit();
        connection.setAutoCommit(false);

        stmt = connection.prepareStatement(
                "INSERT INTO Branches (branchId, balance, branchName, address) " +
                        "VALUES(?, ?, ?, ?)"
        );

        for (int i = 0, max = data.Branches.length; i < max; i++) {
            Branch branch = data.Branches[i];
            stmt.clearParameters();
            stmt.setInt(1, branch.BranchId);
            stmt.setInt(2, branch.Balance);
            stmt.setString(3, branch.Branchname);
            stmt.setString(4, branch.Address);
            stmt.executeUpdate();
        }

        stmt = connection.prepareStatement(
                "INSERT INTO Accounts (accid, balance, branchId, name, address)" +
                        "VALUES (?, ?, ?, ?, ?) ");
        for (int i = 0, max = data.Accounts.length; i < max; i++) {
            Account account = data.Accounts[i];
            stmt.clearParameters();
            stmt.setInt(1, account.AccId);
            stmt.setInt(2, account.Balance);
            stmt.setInt(3, account.BranchId);
            stmt.setString(4, account.Name);
            stmt.setString(5, account.Address);
            stmt.executeUpdate();
        }

        stmt = connection.prepareStatement(
                "INSERT INTO Tellers (tellerId, balance, branchId, tellerName, address)" +
                        "VALUES (?, ?, ?, ?, ?) ");
        for (int i = 0, max = data.Tellers.length; i < max; i++) {
            Teller teller = data.Tellers[i];
            stmt.clearParameters();
            stmt.setInt(1, teller.TellerId);
            stmt.setInt(2, teller.Balance);
            stmt.setInt(3, teller.BranchId);
            stmt.setString(4, teller.Name);
            stmt.setString(5, teller.Address);
            stmt.executeUpdate();
        }

        connection.commit(); // Änderungen absenden
        connection.setAutoCommit(autoCommit); // auto-commit zurücksetzen
    }
}
