package uni.dbi.p7.benchmarks;

import uni.dbi.p7.data.BenchmarkData;
import uni.dbi.p7.data.entities.Account;
import uni.dbi.p7.data.entities.Branch;
import uni.dbi.p7.data.entities.Teller;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Befüllt die Datenbank mithilfe einer einzigen Query.
 * <p/>
 * Dabei wird aus allen Operationen eine einzige große Query
 * erzeugt und zum Server geschickt.
 */
public class SingleStatementBenchmark extends AbstractBenchmark {
    public SingleStatementBenchmark(Connection connection) {
        super(connection);
    }

    //Daten einfügen
    @Override
    protected void insertData(BenchmarkData data) throws SQLException {
        Statement sqlStatement = connection.createStatement();

        //StringBuilder Objekt erstellen
        StringBuilder stmt = new StringBuilder();

        //Es wird ein String mit den Daten aus dem BenchmarkData Objekt erweitert
        stmt.append("INSERT INTO Branches (branchId, balance, branchName, address) VALUES (");
        for (int i = 0, max = data.Branches.length; i < max; i++) {
            Branch branch = data.Branches[i];
            stmt.append(branch.BranchId);
            stmt.append(",");
            stmt.append(branch.Balance);
            stmt.append(",'");
            stmt.append(branch.Branchname);
            stmt.append("','");
            stmt.append(branch.Address);
            stmt.append("'),(");
        }
        //Die letzten zwei Zeichen entfernen
        stmt.setLength(stmt.length() - 2);

        //Der Separator ";" muss gesetzt werden, da die darauf folgende Bedingung nicht als neue INSERT Query erkannt wird
        stmt.append(";INSERT INTO Accounts (accid, balance, branchId, name, address) VALUES (");
        for (int i = 0, max = data.Accounts.length; i < max; i++) {
            Account account = data.Accounts[i];

            stmt.append(account.AccId);
            stmt.append(",");
            stmt.append(account.Balance);
            stmt.append(",");
            stmt.append(account.BranchId);
            stmt.append(",'");
            stmt.append(account.Name);
            stmt.append("','");
            stmt.append(account.Address);
            stmt.append("'),(");
        }
        stmt.setLength(stmt.length() - 2);

        stmt.append(";INSERT INTO Tellers (tellerId, balance, branchId, tellerName, address) VALUES (");
        for (int i = 0, max = data.Tellers.length; i < max; i++) {
            Teller teller = data.Tellers[i];

            stmt.append(teller.TellerId);
            stmt.append(",");
            stmt.append(teller.Balance);
            stmt.append(",");
            stmt.append(teller.BranchId);
            stmt.append(",'");
            stmt.append(teller.Name);
            stmt.append("','");
            stmt.append(teller.Address);
            stmt.append("'),(");
        }
        stmt.setLength(stmt.length() - 2);

        //Query Ausführen
        sqlStatement.executeUpdate(stmt.toString());
    }
}
