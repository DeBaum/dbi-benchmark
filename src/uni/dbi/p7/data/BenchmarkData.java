package uni.dbi.p7.data;

import uni.dbi.p7.data.entities.Account;
import uni.dbi.p7.data.entities.Branch;
import uni.dbi.p7.data.entities.Teller;

/**
 * Speichert für die Benchmarks notwendige Daten
 */
public class BenchmarkData {
    public Account[] Accounts;
    public Branch[] Branches;
    public Teller[] Tellers;

    public BenchmarkData(int n) {
        this.Accounts = new Account[n * 100000];
        this.Branches = new Branch[n];
        this.Tellers = new Teller[n * 10];
    }
}
