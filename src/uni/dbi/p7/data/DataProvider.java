package uni.dbi.p7.data;

import uni.dbi.p7.data.entities.Account;
import uni.dbi.p7.data.entities.Branch;
import uni.dbi.p7.data.entities.Teller;
import uni.dbi.p7.help.BenchmarkHelper;

import java.io.*;

/**
 * Klasse zur Bereitstellung von für die Benchmarks erforderlichen Daten.
 */
public class DataProvider {

    //vordefinierte Werte
    private static final String BRANCH_NAME = "u245dmVobH6ET5cVLgTI";
    private static final String TELLER_NAME = "u245dmVobH6ET5cVLgTB";
    private static final String SHORT_ADDRESS = "FATCVLNCohekO2h0TDRoEhiHpLe6Rr6D1NEkCrsI44bzPTXDDwh3B1TSik0nVvYyJqmE"; // 68
    private static final String LONG_ADDRESS = "dAIOqs3668FVFXqTp92tbELeHc2G03ZwP0nWBt4KVOmKQ7YJ2tbifKuT5t3vAF7bEN7peHKG"; // 72

    private static final String CWD = System.getProperty("user.dir");
    private static final String FILE_DELIMITER = ",";

    /**
     * Erzeugt die für einen Benchmark notwendigen Daten. Falls bereits
     * Daten für das übergebene n generiert wurden, werden diese genommen.
     */
    public BenchmarkData getBenchmarkData(int n) {
        // Existierende Datein suchen
        File dataFolder = new File(CWD + "/data-" + n);
        BenchmarkData data = loadData(dataFolder, n);

        // Neue generieren, falls noch nicht vorhandenen
        if (data == null) {
            data = createData(n);
            System.out.println("Data: Generated");
        } else {
            System.out.println("Data: Cached");
        }

        // Daten speichern
        saveData(data, dataFolder);
        return data;
    }

    /**
     * Liest zuvor gespeicherte Benchmark-Daten für das übergebene n
     * aus den entsprechenden Dateien, falls vorhanden.
     *
     * @param dataFolder Basisverzeichnis für gespeicherte Daten
     * @return Gespeicherte Benchmark-Daten oder null, falls keine existieren
     */
    private BenchmarkData loadData(File dataFolder, int n) {
        // Accounts
        File accountsFile = new File(dataFolder.getAbsolutePath() + "/accounts.txt");
        if (accountsFile.exists()) {
            // Neuen Container für Benchmark-Daten erstellen
            BenchmarkData data = new BenchmarkData(n);
            try {
                BufferedReader br = new BufferedReader(new FileReader(accountsFile));

                // Dateiinhalte Yeilenweise auslesen
                int i = 0;
                for (String line; (line = br.readLine()) != null; ) {
                    String s[] = line.split(FILE_DELIMITER);
                    Account account = new Account();
                    account.AccId = Integer.parseInt(s[0]);
                    account.Address = s[1];
                    account.Balance = Integer.parseInt(s[2]);
                    account.BranchId = Integer.parseInt(s[3]);
                    account.Name = s[4];
                    data.Accounts[i] = account;
                    i++;
                }

                File branchesFile = new File(dataFolder.getAbsolutePath() + "/branches.txt");
                br = new BufferedReader(new FileReader(branchesFile));
                i = 0;
                for (String line; (line = br.readLine()) != null; ) {
                    String s[] = line.split(FILE_DELIMITER);
                    Branch branch = new Branch();
                    branch.Address = s[0];
                    branch.Balance = Integer.parseInt(s[1]);
                    branch.BranchId = Integer.parseInt(s[2]);
                    branch.Branchname = s[3];
                    data.Branches[i] = branch;
                    i++;
                }

                //Gleiches vorgehen wie bei den Accounts
                File tellersFile = new File(dataFolder.getAbsolutePath() + "/tellers.txt");
                br = new BufferedReader(new FileReader(tellersFile));
                i = 0;
                for (String line; (line = br.readLine()) != null; ) {
                    String s[] = line.split(FILE_DELIMITER);
                    Teller teller = new Teller();
                    teller.Address = s[0];
                    teller.Balance = Integer.parseInt(s[1]);
                    teller.BranchId = Integer.parseInt(s[2]);
                    teller.Name = s[3];
                    teller.TellerId = Integer.parseInt(s[4]);
                    data.Tellers[i] = teller;
                    i++;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return data;
        } else return null;
    }

    /**
     * Speichert die übergebenen Daten in die übergebene Datei.
     *
     * @param data     Daten
     * @param dataFile Dateie in die geschrieben werden soll
     */
    private void saveData(BenchmarkData data, File dataFile) {
        try {
            // Notwendige Verzeichnisse anlegen, falls nicht vorhanden
            dataFile.mkdirs();

            // Accounts serialisieren und Zeile für Zeile in die Textdatei schreiben
            BufferedWriter writer = new BufferedWriter(new FileWriter(dataFile.getAbsolutePath() + "/accounts.txt"));
            for (int i = 0, max = data.Accounts.length; i < max; i++) {
                Account account = data.Accounts[i];
                writer.write(String.valueOf(account.AccId) + FILE_DELIMITER
                        + account.Address + FILE_DELIMITER
                        + account.Balance + FILE_DELIMITER
                        + account.BranchId + FILE_DELIMITER
                        + account.Name
                        + "\n");
            }
            writer.close();

            writer = new BufferedWriter(new FileWriter(dataFile.getAbsolutePath() + "/branches.txt"));
            for (int i = 0, max = data.Branches.length; i < max; i++) {
                Branch branch = data.Branches[i];
                writer.write(branch.Address + FILE_DELIMITER
                        + branch.Balance + FILE_DELIMITER
                        + branch.BranchId + FILE_DELIMITER
                        + branch.Branchname
                        + "\n");
            }
            writer.close();

            writer = new BufferedWriter(new FileWriter(dataFile.getAbsolutePath() + "/tellers.txt"));
            for (int i = 0, max = data.Tellers.length; i < max; i++) {
                Teller teller = data.Tellers[i];
                writer.write(teller.Address + FILE_DELIMITER
                        + teller.Balance + FILE_DELIMITER
                        + teller.BranchId + FILE_DELIMITER
                        + teller.Name + FILE_DELIMITER
                        + teller.TellerId
                        + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Neue zufällige Daten generieren.
     */
    private BenchmarkData createData(int n) {
        // Container für Daten erzeugen
        BenchmarkData data = new BenchmarkData(n);

        // Branches generieren
        for (int i = 0; i < n; i++) {
            Branch branch = new Branch();
            data.Branches[i] = branch;

            branch.BranchId = i + 1;
            branch.Balance = 0;
            branch.Branchname = BRANCH_NAME;
            branch.Address = LONG_ADDRESS;
        }

        // Accounts generieren
        for (int i = 0, max = n * 100000; i < max; i++) {
            Account account = new Account();
            data.Accounts[i] = account;

            account.AccId = i + 1;
            account.Balance = 0;
            account.BranchId = BenchmarkHelper.random(1, n);
            account.Name = BRANCH_NAME;
            account.Address = SHORT_ADDRESS;
        }

        // Teller generieren
        for (int i = 0, max = n * 10; i < max; i++) {
            Teller teller = new Teller();
            data.Tellers[i] = teller;

            teller.TellerId = i + 1;
            teller.Balance = 0;
            teller.BranchId = BenchmarkHelper.random(1, n);
            teller.Name = TELLER_NAME;
            teller.Address = SHORT_ADDRESS;
        }

        return data;
    }
}
