package uni.dbi.p7.data.entities;

/**
 * Repräsentiert einen Account
 */
public class Account {
    public int AccId;
    public String Name;
    public int Balance;
    public int BranchId;
    public String Address;
}
