package uni.dbi.p7.data.entities;

/**
 * Repräsentiert einen Branch
 */
public class Branch {
    public int BranchId;
    public String Branchname;
    public int Balance;
    public String Address;
}
