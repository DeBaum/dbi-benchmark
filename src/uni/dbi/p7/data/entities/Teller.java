package uni.dbi.p7.data.entities;

/**
 * Repräsentiert einen Teller
 */
public class Teller {
    public int TellerId;
    public String Name;
    public int Balance;
    public int BranchId;
    public String Address;
}
