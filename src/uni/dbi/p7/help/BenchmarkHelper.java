package uni.dbi.p7.help;

/**
 * Hilfsklasse für den Benchmark
 */
public class BenchmarkHelper {

    /**
     * Zufällige Zahl ermitteln
     */
    public static int random(int min, int max) {
        return (int) (Math.random() * ((max + 1) - min) + min);
    }
}
